import React, { Component } from "react";
import { getBoards, createBoard } from "../api";
import BoardsList from "./BoardsList";
import PopUp from "./PopUp";
import { connect } from "react-redux";

import * as actionType from "../store/actions";

class BoardContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //boards: [],
      show: false,
      //spinner: true,
    };
  }

  //redux-thunk implementation

  componentDidMount() {
    //a function is passed as a parameter to dispatch rather than an object
    this.props.dispatch(fetchProducts());
    // this.props.dispatch({ type: actionType.GET_BOARDS });
    // getBoards()
    //   .then((boards) => {
    //     this.props.dispatch({
    //       type: actionType.GET_BOARDS_SUCCESS,
    //       data: { boards },
    //     });
    //   })
    //   .catch((err) => {
    //     this.props.dispatch({ type: actionType.GET_BOARDS_ERROR });
    //   });
  }

  handleModal = () => {
    this.setState({
      show: !this.state.show,
    });
  };

  handleTitle = (newTitle) => {
    createBoard(newTitle).then((data) => {
      console.log(data);
      this.props.dispatch({
        type: "HANDLE_TITLE",
        payloads: { data: data },
      });
    });
  };

  render() {
    if (this.props.spinner) {
      return <p>Loading...</p>;
    }
    if (this.props.hasError) {
      return <p>Something went wrong</p>;
    }
    return (
      <div className="d-flex flex-column">
        {this.state.show === true ? (
          <PopUp
            toggleShow={this.handleModal}
            handleBoardTitle={this.handleTitle}
          />
        ) : (
          ""
        )}
        <BoardsList
          boardData={this.props.boards}
          toggleShow={this.handleModal}
          showBoard={this.handleShowBoard}
          spinner={this.props.spinner}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    spinner: state.api.spinner,
    boards: state.api.boards,
  };
};

const fetchProducts = () => {
  return (dispatch) => {
    dispatch({ type: actionType.GET_BOARDS });
    return getBoards()
      .then((boards) => {
        dispatch({ type: actionType.GET_BOARDS_SUCCESS, data: { boards } });
        //return boards;
      })
      .catch((error) =>
        dispatch({
          type: actionType.GET_BOARDS_ERROR,
        })
      );
  };
};
export default connect(mapStateToProps)(BoardContainer);
