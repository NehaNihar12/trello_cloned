import React, { Component } from "react";
import { connect } from "react-redux";
import { getLists, createLists, deleteList } from "../api";
import Cards from "./Cards";

import * as actions from "../store/actions";

class Board extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     boardData: [],
  //     addNewList: false,
  //     newListTitle: "",
  //   };
  // }

  //redux-thunk : here actions are functions

  componentDidMount() {
    const id = this.props.match.params.boardId;
    this.props.dispatch(fetchProducts(id));
    // console.log(id);
    // getLists(id).then((data) => {
    //   this.props.dispatch({
    //     type: actions.FETCH_BOARD_DATA,
    //     payloads: { boardData: data },
    //   });
    // });
  }

  handleNewList = () => {
    this.props.dispatch({
      type: actions.ADD_NEW_LIST,
    });
  };

  handleNewListTitle = (e) => {
    this.props.dispatch({
      type: actions.NEW_LIST_TITLE,
      payloads: { value: e.target.value },
    });
  };

  createNewList = (id, title) => {
    if (title !== "") {
      createLists(id, title).then((data) => {
        this.props.dispatch({
          type: actions.CREATE_NEW_LIST,
          payloads: {
            data: data,
            newListTitle: "",
          },
        });
      });
    }
  };

  deleteList = (id) => {
    deleteList(id).then((res) => {
      console.log(res);
      this.props.dispatch({
        type: actions.DELETE_LIST,
        payloads: { id: id },
      });
    });
  };

  render() {
    //addNewList;
    return (
      <div className="d-flex ListContainer">
        {this.props.boardData.map((list) => {
          return (
            <div className="Lists" key={list.id}>
              <div className="d-flex flex-column p-3 m-2 fw-bold List">
                <div className="d-inline">
                  <span>{list.name}</span>
                  <button
                    title="Delete List"
                    onClick={() => this.deleteList(list.id)}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="currentColor"
                      className="bi bi-trash"
                      viewBox="0 0 16 16"
                    >
                      <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                      <path
                        fillRule="evenodd"
                        d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"
                      />
                    </svg>
                  </button>
                </div>
                <div>{<Cards listData={list} />}</div>
              </div>
            </div>
          );
        })}

        {this.props.addNewList === false ? (
          <div>
            <div className="d-flex rounded justify-content-start align-items-end p-3 mx-2 fw-bold NewList">
              <p className="px-2" onClick={this.handleNewList}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  className="bi bi-plus-square"
                  viewBox="0 0 16 16"
                >
                  <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z" />
                  <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                </svg>
                <span className="mx-2">Add another list</span>
              </p>
            </div>
          </div>
        ) : (
          <div>
            <form
              className="mx-2 p-3 rounded newForm"
              onSubmit={(e) => {
                e.preventDefault();
                this.createNewList(
                  this.props.match.params.boardId,
                  this.props.newListTitle
                );
              }}
            >
              <input
                type="text"
                placeholder="Enter list title"
                value={this.props.newListTitle}
                onChange={this.handleNewListTitle}
                autoFocus
              />
              <div>
                <button
                  type="submit"
                  className="my-3 border-0 fw-bold text-white rounded newListBtn"
                >
                  Add List
                </button>
                <button
                  className="my-3 border-0 fw-bold text-white rounded bg-secondary close"
                  onClick={this.handleNewList}
                >
                  Close
                </button>
              </div>
            </form>
          </div>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    boardData: state.Board.boardData,
    addNewList: state.addNewList,
  };
};
const fetchProducts = (id) => {
  return (dispatch) => {
    //dispatch({ type: actionType.GET_BOARDS });
    return getLists(id).then((data) => {
      this.props.dispatch({
        type: actions.FETCH_BOARD_DATA,
        payloads: { boardData: data },
      });
    });
  };
};

export default connect(mapStateToProps)(Board);
