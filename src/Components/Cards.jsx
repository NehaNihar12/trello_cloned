import React, { Component } from "react";
import { connect } from "react-redux";
import { createCard, getCards, deleteCard } from "../api";
import EachCard from "./EachCard";
import * as actionType from "../store/actions";

class Cards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggleModal: false,
    };
  }

  componentDidMount() {
    const id = this.props.listData.id;
    getCards(id).then(
      (data) =>
        this.props.dispatch({
          type: actionType.FETCH_CARD_DATA,
          payloads: { cardData: data },
        })
      // .then(() => {
      //   this.setState({
      //     isDataLoaded: true,
      //   });
      // })
    );
  }

  handleNewTitle = (e) => {
    const newTitle = e.target.value;
    this.props.dispatch({
      type: actionType.HANDLE_CARDS_TITLE,
      payloads: {
        newCardTitle: newTitle,
      },
    });
  };

  handleNewCard = () => {
    this.props.dispatch({
      type: actionType.HANDLE_NEW_CARD,
    });
  };

  createNewCard = (id, title) => {
    createCard(id, title).then((data) => {
      this.props.dispatch({
        type: actionType.CREATE_NEW_CARD,
        payloads: { id: id, data: data },
      });
    });
  };

  deleteCard = (id) => {
    deleteCard(id).then((res) => {
      // console.log(res);
      this.props.dispatch({
        type: actionType.DELETE_CARD,
        payloads: { id: id },
      });
    });
  };

  handleModal = () => {
    this.setState({
      toggleModal: !this.state.toggleModal,
    });
  };

  render() {
    return (
      <div key={this.props.listData.id} className="flex flex-column my-2">
        {this.state.isDataLoaded === true
          ? this.props.cardData.map((card) => {
              return (
                <EachCard
                  key={card.id}
                  card={card}
                  handleDelete={this.deleteCard}
                  listInfo={this.props.listData}
                />
              );
            })
          : ""}
        <div className="d-flex justify-content-between mt-2">
          {this.props.addNewCard === false ? (
            <span className="rounded m-1 AddCard">
              <button
                title="Add new"
                onClick={this.handleNewCard}
                className="rounded"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  className="bi bi-plus-circle text-white m-1"
                  viewBox="0 0 16 16"
                >
                  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                  <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                </svg>{" "}
                <span className="fw-bold text-white">Add a card</span>
              </button>
            </span>
          ) : (
            <form
              className="d-flex flex-column"
              onSubmit={(e) => {
                e.preventDefault();
                this.createNewCard(
                  this.props.listData.id,
                  this.props.newCardTitle
                );
              }}
            >
              <textarea
                placeholder="Enter title for this card"
                className="form-control"
                autoFocus
                value={this.props.newCardTitle}
                onChange={this.handleNewTitle}
              />
              <section className="d-flex justify-content-between mt-3">
                <span className="rounded AddCard">
                  <button title="Add new" type="submit" className="rounded ">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="currentColor"
                      className="bi bi-plus-circle text-white m-1"
                      viewBox="0 0 16 16"
                    >
                      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                      <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                    </svg>{" "}
                    <span className="fw-bold text-white">Add</span>
                  </button>
                </span>
                <button
                  onClick={this.handleNewCard}
                  className="btn-secondary bg-secondary text-white fw-bold rounded"
                >
                  Close
                </button>
              </section>
            </form>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cardData: state.cards.cardData,
    addNewCard: state.cards.addNewCard,
    newCardTitle: state.cards.newCardTitle,
  };
};
export default connect(mapStateToProps)(Cards);
