import React, { Component } from "react";
import { getCheckItem, createCheckItem, deleteCheckItem } from "../api";
import CheckItem from "./CheckItem";
import { Form } from "react-bootstrap";
import { connect } from "react-redux";

class CheckList extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     checkItemTitle: "",
  //     checkItemData: [],
  //     addItem: false,
  //     spinner: true,
  //   };
  // }

  componentDidMount() {
    // console.log(this.props.checkListData.id);
    this.props.dispatch({ type: "GET_CHECKED" });
    getCheckItem(this.props.checkListData.id)
      .then((checkData) =>
        this.props.dispatch({
          type: "GET_CHECKED_SUCCESS",
          data: {
            checkItemData: checkData,
          },
        })
      )
      .catch((err) => {
        this.props.dispatch({ type: "GET_CHECKED_ERROR" });
      });
  }

  handleTitle = (e) => {
    let checkItemTitle = e.target.value;
    this.props.dispatch({
      type: "ADD_TITLE",
      payloads: { checkItemTitle: checkItemTitle },
    });
  };

  handleAddItem = () => {
    this.props.dispatch({
      type: "ADD_ITEM",
      payloads: { addItem: !this.props.addItem },
    });
  };

  createCheckItem = (id, title) => {
    createCheckItem(id, title).then((data) => {
      this.props.dispatch({
        type: "CREATE_CHECK_ITEM",
        payloads: {
          checkItemData: [...this.props.checkItemData, data],
          addItem: !this.state.addItem,
          checkItemTitle: "",
        },
      });
    });
  };

  handleDelete = (listId, itemId) => {
    deleteCheckItem(listId, itemId).then((res) => {
      this.props.dispatch({
        type: "DELETE",
        payloads: {
          checkItemData: this.state.checkItemData.filter(
            (item) => item.id !== itemId
          ),
        },
      });
    });
  };

  handleUpdate = (data) => {
    this.props.dispatch({
      type: "UPDATE",
      payloads: {
        checkItemData: this.state.checkItemData.map((item) =>
          item.id === data.id ? data : item
        ),
      },
    });
  };

  render() {
    if (this.props.spinner) {
      return <p>Looading...</p>;
    }
    if (this.props.hasError) {
      return <p>Something went wrong</p>;
    }
    return (
      <section className="p-2 m-3 rounded fw-bolder CheckLists">
        <div className="d-flex flex-column m-3 justify-content-between ChecklistContainer">
          <div className="d-flex justify-content-between CheckList">
            <span>{this.props.checkListData.name}</span>
            <button
              onClick={() =>
                this.props.deleteCheckList(this.props.checkListData.id)
              }
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                fill="currentColor"
                class="bi bi-trash"
                viewBox="0 0 16 16"
              >
                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"></path>
                <path
                  fill-rule="evenodd"
                  d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"
                ></path>
              </svg>
            </button>
          </div>

          <div className="d-flex flex-column CheckItems">
            {this.props.checkItemData.map((item) => (
              <CheckItem
                className="d-flex flex-column"
                key={item.id}
                cardID={this.props.cardId}
                listID={this.props.checkListData.id}
                checkItemData={item}
                deleteCheckItem={this.handleDelete}
                updateItem={this.handleUpdate}
              />
            ))}
            {this.props.addItem === true ? (
              <section className="newCheckList">
                <Form
                  className="d-flex flex-column p-2 justify-content-between "
                  onSubmit={(e) => {
                    e.preventDefault();
                    this.createCheckItem(
                      this.props.checkListData.id,
                      this.props.checkItemTitle
                    );
                  }}
                >
                  <input
                    type="text"
                    autoFocus
                    placeholder="Enter checkitem title"
                    value={this.props.checkItemTitle}
                    onChange={this.handleTitle}
                  />
                  <div className="m-2">
                    <button
                      className="m-2 border-0 bg-transparent fw-bold"
                      onClick={this.handleDelete}
                    >
                      Create
                    </button>
                  </div>
                </Form>
              </section>
            ) : (
              <button
                className="m-2 rounded border-0 text-white fw-bold itemBtn"
                onClick={this.handleAddItem}
              >
                + Add Item
              </button>
            )}
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    checkItemTitle: state.checklist.checkItemTitle,
    checkItemData: state.checklist.checkItemData,
    addItem: state.checklist.addItem,
    hasError: state.checklist.hasError,
    spinner: state.checklist.spinner,
  };
};
export default connect(mapStateToProps)(CheckList);
