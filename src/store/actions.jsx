//boardContainer
export const GET_BOARDS = "GET_BOARDS";
export const GET_BOARDS_SUCCESS = "GET_BOARDS_SUCCESS";
export const GET_BOARDS_ERROR = "GET_BOARDS_ERROR";
export const HANDLE_MODAL = "HANDLE_MODAL";
//board.jsx
export const FETCH_BOARD_DATA = "BOARD_DATA";
export const ADD_NEW_LIST = "ADD_NEW_LIST";
export const NEW_LIST_TITLE = "NEW_LIST_TITLE";
export const DELETE_LIST = "DELETE_LIST";
export const CREATE_NEW_LIST = "CREATE_NEW_LIST";

//cards.jsx
export const FETCH_CARD_DATA = "FETCH_CARD_DATA";
export const HANDLE_CARDS_TITLE = "CARDS_TITLE";
export const HANDLE_NEW_CARD = "HANDLE_NEW_CARD";
export const CREATE_NEW_CARD = "CREATE_NEW_CARD";
export const DELETE_CARD = "DELETE_CARD";
