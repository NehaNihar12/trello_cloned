//import * as actionType from "../actions";

export default function checklist(
  state = {
    checkItemTitle: "",
    checkItemData: [],
    addItem: false,
    hasError: false,
    spinner: true,
  },
  action
) {
  console.log(state, action);
  switch (action.type) {
    case "GET_CHECKED":
      return {
        ...state,
        checkItemData: [],
        hasError: false,
        spinner: true,
      };

    case "GET_CHECKED_SUCCESS":
      return {
        ...state,
        checkItemData: action.data.checkItemData,
        hasError: false,
        spinner: false,
      };
    case "GET_CHECKED_ERROR":
      return {
        ...state,
        hasError: true,
        checkItemData: [],
        spinner: false,
      };
    //....

    case "ADD_TITLE":
      return {
        ...state,
        checkItemTitle: action.payloads.checkItemTitle,
      };
    case "ADD_ITEM":
      return {
        ...state,
        addItem: action.payloads.addItem,
      };
    case "CREATE_CHECK_ITEM":
      return {
        ...state,
        checkItemData: action.payloads.checkItemData,
        addItem: action.payloads.addItem,
        checkItemTitle: action.payloads.checkItemTitle,
      };
    case "DELETE":
      return {
        ...state,
        checkItemData: action.payloads.checkItemData,
      };
    case "UPDATE":
      return {
        ...state,
        checkItemData: action.payloads.checkItemData,
      };
    default:
      return state;
  }
}
