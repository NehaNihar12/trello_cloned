import * as actionType from "../actions";

export default function apiReducer(
  state = { spinner: true, boards: [], hasError: false, show: false },
  action
) {
  console.log(state, action);
  switch (action.type) {
    case actionType.GET_BOARDS:
      return {
        ...state,
        spinner: true,
        boards: [],
        hasError: false,
      };

    case "GET_BOARDS_SUCCESS":
      return {
        ...state,
        spinner: false,
        boards: action.data.boards,
        hasError: false,
      };
    case "GET_BOARDS_ERROR":
      return {
        ...state,
        spinner: false,
        hasError: true,
        boards: [],
      };

    case "HANDLE_TITLE":
      return {
        ...state,
        boards: [...state.boards, action.payloads.data],
      };
    default:
      return state;
  }
}
