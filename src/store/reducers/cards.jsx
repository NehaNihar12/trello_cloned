import * as actionType from "../actions";

export default function cards(
  state = {
    cardData: {},
    addNewCard: false,
    newCardTitle: "",
    toggleModal: false,
    isDataLoaded: false,
  },
  action
) {
  switch (action.type) {
    case actionType.FETCH_CARD_DATA: {
      return {
        ...state,
        cardData: action.payloads.cardData,
        isDataLoaded: true,
      };
    }
    case actionType.HANDLE_CARDS_TITLE:
      return {
        ...state,
        newCardTitle: action.payloads.newCardTitle,
      };
    case actionType.HANDLE_NEW_CARD:
      return {
        addNewCard: !state.addNewCard,
      };
    case actionType.CREATE_NEW_CARD: {
      console.log(state.cardData);
      return {
        ...state,
        newCardTitle: "",
        cardData: { ...cards, [action.payloads.id]: action.payloads.data },
        addNewCard: !state.addNewCard,
      };
    }
    case actionType.DELETE_CARD:
      return {
        ...state,
        cardData: state.cardData.filter(
          (card) => card.id !== action.payloads.id
        ),
      };
    default:
      return state;
  }
}
