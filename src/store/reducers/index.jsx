import { combineReducers } from "redux";
import apiReducer from "./api";
import checklist from "./checklist";
import BoardReducer from "./BoardReducer";
import cards from "./cards";

export default combineReducers({
  Board: BoardReducer,
  api: apiReducer,
  checklist: checklist,
  cards: cards,
});
