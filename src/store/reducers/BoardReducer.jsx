import * as actionType from "../../store/actions";
export default function BoardReducer(
  state = { boardData: [], addNewList: false, newListTitle: "" },
  action
) {
  switch (action.type) {
    case actionType.FETCH_BOARD_DATA: {
      return {
        ...state,
        boardData: action.payloads.boardData,
      };
    }
    case actionType.ADD_NEW_LIST:
      return {
        ...state,
        addNewList: !state.addNewList,
      };
    case actionType.NEW_LIST_TITLE:
      return {
        ...state,
        newListTitle: action.payloads.value,
      };
    case actionType.CREATE_NEW_LIST:
      return {
        ...state,
        boardData: [action.payloads.data, ...state.boardData],
        newListTitle: action.payloads.newListTitle,
        addNewList: !state.addNewList,
      };
    case actionType.DELETE_LIST:
      return {
        ...state,
        boardData: state.boardData.filter(
          (list) => list.id !== action.payloads.id
        ),
      };
    default:
      return state;
  }
}
