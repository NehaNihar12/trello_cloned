import axios from "axios";

const API_KEY = "8e3b65635b6db44cfd994ad1251cfc9f";
const TOKEN =
  "85e2b3a17a57ea16aca6b64830936597fcaf8d23761370be12980b85d80c2e44";

export function getBoards() {
  return axios
    .get(
      `https://api.trello.com/1/members/me/boards?fields=name,url&key=${API_KEY}&token=${TOKEN}`
    )
    .then((res) => res.data);
}

export function createBoard(title) {
  return axios
    .post(
      `https://api.trello.com/1/boards/?name=${title}&key=${API_KEY}&token=${TOKEN}`
    )
    .then((res) => res.data);
}

export function getLists(id) {
  return axios
    .get(
      `https://api.trello.com/1/boards/${id}/lists?key=${API_KEY}&token=${TOKEN}`
    )
    .then((res) => res.data);
}

export function createLists(id, title) {
  return axios
    .post(
      `https://api.trello.com/1/boards/${id}/lists?name=${title}&key=${API_KEY}&token=${TOKEN}`
    )
    .then((res) => res.data);
}

export function deleteList(id) {
  return axios.put(
    `https://api.trello.com/1/lists/${id}/closed?value=true&key=${API_KEY}&token=${TOKEN}`
  );
}

export function getCards(id) {
  return axios
    .get(
      `https://api.trello.com/1/lists/${id}/cards?key=${API_KEY}&token=${TOKEN}`
    )
    .then((res) => res.data);
}
export function createCard(id, title) {
  return axios
    .post(
      `https://api.trello.com/1/cards?name=${title}&idList=${id}&key=${API_KEY}&token=${TOKEN}`
    )
    .then((res) => res.data);
}

export function deleteCard(id) {
  return axios.delete(
    `https://api.trello.com/1/cards/${id}?key=${API_KEY}&token=${TOKEN}`
  );
}

export function getCheckList(id) {
  return axios
    .get(
      `https://api.trello.com/1/cards/${id}/checklists?key=${API_KEY}&token=${TOKEN}`
    )
    .then((res) => res.data);
}

export function createCheckList(id, title) {
  return axios
    .post(
      `https://api.trello.com/1/checklists?name=${title}&idCard=${id}&key=${API_KEY}&token=${TOKEN}`
    )
    .then((res) => res.data);
}

export function deleteCheckList(id) {
  return axios.delete(
    `https://api.trello.com/1/checklists/${id}?key=${API_KEY}&token=${TOKEN}`
  );
}

export function getCheckItem(id) {
  return axios
    .get(
      `https://api.trello.com/1/checklists/${id}/checkItems?key=${API_KEY}&token=${TOKEN}`
    )
    .then((res) => res.data);
}

export function createCheckItem(id, title) {
  return axios
    .post(
      `https://api.trello.com/1/checklists/${id}/checkItems?name=${title}&key=${API_KEY}&token=${TOKEN}`
    )
    .then((res) => res.data);
}

export function deleteCheckItem(listId, itemId) {
  return axios.delete(
    `https://api.trello.com/1/checklists/${listId}/checkItems/${itemId}?key=${API_KEY}&token=${TOKEN}`
  );
}

export function updateCheckItem(cardId, itemId, status) {
  return axios
    .put(
      `https://api.trello.com/1/cards/${cardId}/checkItem/${itemId}?state=${status}&key=${API_KEY}&token=${TOKEN}`
    )
    .then((res) => res.data);
}
