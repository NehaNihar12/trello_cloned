import "../src/App.css"
import Header from './Components/Header'
import BoardsContainer from './Components/BoardsContainer';
import Board from "./Components/Board";


import { Switch, Route, Redirect } from 'react-router-dom';

function App() {
  return (
    <div className="App"  >
      <Header />
      <Switch>
        <Route exact path="/">
          <Redirect to='/boards' />
        </Route>
        <Route exact path='/boards' component={BoardsContainer} />
        <Route exact path='/boards/:boardId' component={Board} />
      </Switch>
    </div>
  );
}

export default App;
